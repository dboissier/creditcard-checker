# Credit card checker

[![Build Status](https://travis-ci.org/dboissier/CreditCardChecker.png?branch=master)](https://travis-ci.org/dboissier/CreditCardChecker)

### What is this?

This small program is an exercice to validate credit card number with specified checksum algorithm.

Supported algorithm are:
* LUHN

Supported files are:
* CSV (comma separated value)
* TSV (tab separated value)
* PSV (tab separated value)
* XML

### How to build it?

* run the instruction `maven clean install` from the project root directory and you will get an archive `CreditCardCheck-XX-distribution.zip` located in the `target` directory

### How to run it?

* unpack the archive *CreditCardCheck-XX-distribution.zip* in a directory
* in this directory, run `java -jar *CreditCardCheck-XX.jar`. It should display help

### How to extend it?

* File support : you may need to implement the `CreditCardExtractor` interface and register it in the HashMap `extractorByFileFormat` from `CreditCardChecker` class
* Checksum algorithm support : you may need to implement the `ChecksumFormula` interface and register it in the HashMap `formulasByName` from `FormulaRepository` class
* Output method : you may need to extends the `ResultWriter` class and register it in the HashMap `resultWriterByName` from 'Main' class

Of course, do not forget to add unit tests to ensure yours extensions work fine :)

### Legal notice

This software is under Apache license 2.

### Changelog

#### 0.2
* Add result writer extension

#### 0.1
* initial version