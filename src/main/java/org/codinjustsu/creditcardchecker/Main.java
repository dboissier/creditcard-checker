package org.codinjustsu.creditcardchecker;

import org.apache.commons.cli.*;
import org.codinjustsu.creditcardchecker.writer.ConsoleResultWriter;
import org.codinjustsu.creditcardchecker.writer.FileResultWriter;
import org.codinjustsu.creditcardchecker.writer.ResultWriter;

import java.util.HashMap;
import java.util.Map;

public class Main {

    private static Options options = new Options();

    private static final String CURRENT_VERSION = "0.2"; //TODO ugly to do something

    private static final String DEFAULT_FILE_ENCODING = "UTF8";

    private static final Map<String, ResultWriter> resultWriterByName = new HashMap<String, ResultWriter>();

    static {
        buildResultWriters();
        buildOptions();
    }

    private static void buildResultWriters() {
        resultWriterByName.put("console", new ConsoleResultWriter());
        resultWriterByName.put("file", new FileResultWriter());
    }

    private static void buildOptions() {
        options.addOption(Option.builder("f")
                .longOpt("file")
                .required()
                .numberOfArgs(1)
                .desc("file to process")
                .build());

        options.addOption(Option.builder("e")
                .longOpt("encoding")
                .numberOfArgs(1)
                .desc(String.format("file encoding (default is %s)", DEFAULT_FILE_ENCODING))
                .build());

        options.addOption(Option.builder("o")
                .longOpt("output")
                .numberOfArgs(1)
                .desc("result output method [file|console] (default is file)")
                .build());

        options.addOption(Option.builder("h")
                .longOpt("help")
                .desc("print this help")
                .build());
    }

    public static void main(String[] args) {
        CommandLineParser parser = new DefaultParser();
        ResultWriter resultWriter = null;
        try {
            CommandLine line = parser.parse(options, args);
            if (line.hasOption("h")) {
                printHelp();
            }

            resultWriter = getResultWriter(line.getOptionValue("o"));
            resultWriter.enter();
            new CreditCardChecker
                    (line.getOptionValue("f"),
                            getEncoding(line.getOptionValue("e")),
                            resultWriter).checkFile();
        } catch (ParseException parseEx) {
            printHelp();
        } finally {
            if (resultWriter != null) {
                resultWriter.exit();
            }
        }
    }

    private static String getEncoding(String encoding) {
        return encoding != null ? encoding : DEFAULT_FILE_ENCODING;
    }

    private static ResultWriter getResultWriter(String outputResultWriterType) {
        if (resultWriterByName.containsKey(outputResultWriterType)) {
            return resultWriterByName.get(outputResultWriterType);
        } else {
            return resultWriterByName.get("file");
        }
    }

    private static void printHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.setWidth(120);
        formatter.printHelp(
                String.format("java -jar %s-%s.jar -f <filename> [-e <encoding>] -[-o <outputMethod>]",
                        CreditCardChecker.class.getSimpleName(),
                        CURRENT_VERSION),
                options);
    }
}
