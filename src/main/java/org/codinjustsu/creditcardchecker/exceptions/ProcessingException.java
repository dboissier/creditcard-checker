package org.codinjustsu.creditcardchecker.exceptions;

public class ProcessingException extends Exception {

    public ProcessingException(Throwable cause) {
        super(cause);
    }
}
