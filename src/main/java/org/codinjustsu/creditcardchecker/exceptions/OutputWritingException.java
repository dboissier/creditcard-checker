package org.codinjustsu.creditcardchecker.exceptions;

public class OutputWritingException extends RuntimeException {

    public OutputWritingException(Throwable throwable) {
        super(throwable);
    }
}
