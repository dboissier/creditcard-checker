package org.codinjustsu.creditcardchecker;

public class CreditCardEntry {

    public final String number;
    public final String formula;

    public CreditCardEntry(String number, String formula) {
        this.number = number;
        this.formula = formula;
    }
}
