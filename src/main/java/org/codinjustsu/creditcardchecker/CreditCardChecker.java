package org.codinjustsu.creditcardchecker;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.codinjustsu.creditcardchecker.exceptions.ProcessingException;
import org.codinjustsu.creditcardchecker.exceptions.ValidationException;
import org.codinjustsu.creditcardchecker.formula.FormulaRepository;
import org.codinjustsu.creditcardchecker.extractor.*;
import org.codinjustsu.creditcardchecker.writer.ResultWriter;

import java.io.File;
import java.util.*;
import java.util.regex.Pattern;

public class CreditCardChecker {

    private static SortedMap<String, CreditCardExtractor> extractorByFileFormat = new TreeMap<String, CreditCardExtractor>();

    static {
        extractorByFileFormat.put("csv", new SeparatedValueExtractor(","));
        extractorByFileFormat.put("psv", new SeparatedValueExtractor(Pattern.quote("|")));
        extractorByFileFormat.put("tsv", new SeparatedValueExtractor("\t"));
        extractorByFileFormat.put("xml", new XMLExtractor());
    }

    private final String filename;
    private final String encoding;
    private final ResultWriter resultWriter;

    public CreditCardChecker(String filename, String encoding, ResultWriter resultWriter) {
        this.filename = filename;
        this.encoding = encoding;
        this.resultWriter = resultWriter;
    }

    public void checkFile() {
        CreditCardExtractor strategy = getExtractorStrategy(FilenameUtils.getExtension(filename));
        try {
            List<CreditCardEntry> creditCards = strategy.extractCreditCards(new File(filename), encoding);
            processCreditCards(creditCards, resultWriter);
        } catch (ProcessingException ex) {
            System.err.println(ex.getMessage());
        }
    }

    private static CreditCardExtractor getExtractorStrategy(String extension) {
        if (extractorByFileFormat.containsKey(extension)) {
            return extractorByFileFormat.get(extension);
        }
        throw new IllegalArgumentException(String.format("%s file is unsupported (should be %s)", extension, buildExtensionsList()));
    }

    private static void processCreditCards(List<CreditCardEntry> creditCards, ResultWriter resultWriter) {
        for (CreditCardEntry creditCard : creditCards) {
            processContent(creditCard, resultWriter);
        }
    }

    private static void processContent(CreditCardEntry creditCardEntry, ResultWriter resultWriter) {
        String formula = creditCardEntry.formula;
        String number = creditCardEntry.number;
        try {
            FormulaRepository
                    .findFormula(formula)
                    .validate(Utils.convertNumberAsStringToArrayOfDigits(number));

            resultWriter.write(String.format("%s|%s|%s", number, formula, "PASS"));
        } catch (ValidationException ex) {
            resultWriter.write(String.format("%s|%s|%s(%s)", number, formula, "FAIL", ex.getMessage()));
        }
    }

    private static String buildExtensionsList() {
        return StringUtils.join(extractorByFileFormat.keySet(), ", ");
    }

}
