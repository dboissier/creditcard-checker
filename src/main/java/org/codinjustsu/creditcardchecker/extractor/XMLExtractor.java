package org.codinjustsu.creditcardchecker.extractor;

import org.apache.commons.io.FileUtils;
import org.codinjustsu.creditcardchecker.CreditCardEntry;
import org.codinjustsu.creditcardchecker.exceptions.ProcessingException;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class XMLExtractor implements CreditCardExtractor {

    private static XMLInputFactory factory = XMLInputFactory.newInstance();

    private static final int FLAG_NONE = 0;
    private static final int FLAG_NUMBER = 1;
    private static final int FLAG_FORMULA = 2;

    public List<CreditCardEntry> extractCreditCards(File file, String encoding) throws ProcessingException {
        try {
            List<CreditCardEntry> creditCards = new LinkedList<CreditCardEntry>();
            XMLStreamReader reader = factory.createXMLStreamReader(FileUtils.openInputStream(file), encoding);
            while (reader.hasNext()) {
                int type = reader.next();
                switch (type) {
                    case XMLStreamReader.START_ELEMENT:
                        if ("creditCard".equals(reader.getLocalName())) {
                            creditCards.add(processCreditCardElement(reader));
                        }
                        break;
                }
            }
            return creditCards;
        } catch (XMLStreamException xmlSteamEx) {
            throw new ProcessingException(xmlSteamEx);
        } catch (FileNotFoundException fileNotFoundEx) {
            throw new ProcessingException(fileNotFoundEx);
        } catch (IOException ioEx) {
            throw new ProcessingException(ioEx);
        }
    }

    private static CreditCardEntry processCreditCardElement(XMLStreamReader reader) throws XMLStreamException {

        int flag = 0;

        boolean done = false;
        String creditCardNumber = null;
        String formula = null;
        while (reader.hasNext() && !done) {
            int type = reader.next();

            switch (type) {
                case XMLStreamReader.START_ELEMENT:
                    flag = getFlag(reader.getLocalName());
                    break;
                case XMLStreamConstants.CHARACTERS:
                    String elementText = reader.getText();
                    switch (flag) {
                        case FLAG_NUMBER:
                            if (!(reader.isWhiteSpace()))
                                creditCardNumber = elementText;
                            break;
                        case FLAG_FORMULA:
                            if (!(reader.isWhiteSpace()))
                                formula = elementText;
                            break;
                    }
                    break;
                case XMLStreamConstants.END_ELEMENT:
                    if (reader.getLocalName().equals("creditCard")) {
                        done = true;
                    }
                    break;
            }
        }

        return new CreditCardEntry(creditCardNumber, formula);
    }

    private static int getFlag(String elementName) {
        int flag;
        if ("number".equals(elementName)) {
            flag = FLAG_NUMBER;
        } else if ("formula".equals(elementName)) {
            flag = FLAG_FORMULA;
        } else {
            flag = FLAG_NONE;
        }
        return flag;
    }
}
