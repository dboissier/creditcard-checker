package org.codinjustsu.creditcardchecker.extractor;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.apache.commons.lang.StringUtils;
import org.codinjustsu.creditcardchecker.CreditCardEntry;
import org.codinjustsu.creditcardchecker.exceptions.ProcessingException;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class SeparatedValueExtractor implements CreditCardExtractor {

    private final String separator;

    public SeparatedValueExtractor(String separator) {
        this.separator = separator;
    }

    public List<CreditCardEntry> extractCreditCards(File file, String encoding) throws ProcessingException {
        try {
            List<CreditCardEntry> creditCards = new LinkedList<CreditCardEntry>();
            LineIterator lineIterator = FileUtils.lineIterator(file, encoding);
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                if (StringUtils.isBlank(line)) {
                    continue;
                }
                creditCards.add(parse(line));
            }
            return creditCards;
        } catch (IOException ex) {
            throw new ProcessingException(ex);
        }
    }

    protected CreditCardEntry parse(String line) {
        String[] numberAndForumula = line.split(this.separator);
        return new CreditCardEntry(numberAndForumula[0], numberAndForumula[1]);
    }

}
