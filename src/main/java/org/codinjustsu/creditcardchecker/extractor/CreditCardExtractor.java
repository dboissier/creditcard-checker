package org.codinjustsu.creditcardchecker.extractor;


import org.codinjustsu.creditcardchecker.CreditCardEntry;
import org.codinjustsu.creditcardchecker.exceptions.ProcessingException;

import java.io.File;
import java.util.List;

public interface CreditCardExtractor {

    List<CreditCardEntry> extractCreditCards(File file, String encoding) throws ProcessingException;
}
