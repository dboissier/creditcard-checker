package org.codinjustsu.creditcardchecker.extractor;

import org.codinjustsu.creditcardchecker.exceptions.ValidationException;

public class Utils {

    private static final String NUMBER_REGEX = "\\d+";

    public static int[] convertNumberAsStringToArrayOfDigits(String numberAsString) {
        if (!numberAsString.matches(NUMBER_REGEX)) {
            throw new ValidationException(String.format("'%s' is not a number", numberAsString));
        }


        char[] digitsAsStrings = numberAsString.toCharArray();
        int[] result = new int[digitsAsStrings.length];
        for (int index = 0; index < digitsAsStrings.length; index++) {
            char digit = digitsAsStrings[index];
            result[index] = Integer.parseInt(String.valueOf(digit));
        }
        return result;
    }
}
