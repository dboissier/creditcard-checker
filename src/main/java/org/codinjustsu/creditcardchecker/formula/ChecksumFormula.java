package org.codinjustsu.creditcardchecker.formula;


public interface ChecksumFormula {

    void validate(int[] inputDigits);
}
