package org.codinjustsu.creditcardchecker.formula;

import org.codinjustsu.creditcardchecker.exceptions.ValidationException;

import java.util.Arrays;

public class LuhnFormula implements ChecksumFormula {

    public void validate(int[] inputDigits) {
        int result = executeFormula(inputDigits, 0);
        if ((result % 10) != 0) {
            throw new ValidationException("incorrect credit card number");
        }
    }

    static int executeFormula(int[] digits, int digitPosition) {
        int numberOfDigits = digits.length;
        if (numberOfDigits == 0) {
            return 0;
        }
        if (numberOfDigits == 1) {
            return digits[0];
        }
        return executeFormula(Arrays.copyOfRange(digits, 0, numberOfDigits - 1), digitPosition + 1)
                + doubleDigitIfNeeded(digits[numberOfDigits - 1], digitPosition);
    }


    private static int doubleDigitIfNeeded(int digit, int position) {
        return isEven(position) ? base10(digit * 2) : digit;
    }

    private static boolean isEven(int index) {
        return (index % 2) != 0;
    }

    static int base10(int number) {
        return (number / 10) + (number % 10);
    }
}
