package org.codinjustsu.creditcardchecker.formula;

import org.codinjustsu.creditcardchecker.exceptions.ValidationException;

import java.util.HashMap;
import java.util.Map;

public class FormulaRepository {

    private static Map<String, ChecksumFormula> formulasByName = new HashMap<String, ChecksumFormula>();

    static {
        formulasByName.put("LUHN", new LuhnFormula());
    }

    public static ChecksumFormula findFormula(String name) {
        if (formulasByName.containsKey(name)) {
            return formulasByName.get(name);
        }
        throw new ValidationException(String.format("'%s' formula is unsupported", name));
    }
}
