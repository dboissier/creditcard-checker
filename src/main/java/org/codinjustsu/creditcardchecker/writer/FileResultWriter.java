package org.codinjustsu.creditcardchecker.writer;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.codinjustsu.creditcardchecker.exceptions.OutputWritingException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileResultWriter extends ResultWriter {
    private final File outputFile;
    private FileOutputStream fileOutputStream;

    public FileResultWriter() {
        this.outputFile =  new File("result.txt");
    }

    public void write(String data) {
        try {
            IOUtils.write(data + "\n", fileOutputStream);
        } catch (IOException ex) {
            throw new OutputWritingException(ex);
        }
    }


    public void enter() {
        try {
            fileOutputStream = FileUtils.openOutputStream(outputFile);
        } catch (IOException ex) {
            throw new OutputWritingException(ex);
        }
    }

    public void exit() {
        System.out.println(String.format("Results are in %s", outputFile.toString()));
        IOUtils.closeQuietly(fileOutputStream);
    }
}
