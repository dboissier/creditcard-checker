package org.codinjustsu.creditcardchecker.writer;

public class ConsoleResultWriter extends ResultWriter {

    public void write(String data) {
        System.out.println(data);
    }
}
