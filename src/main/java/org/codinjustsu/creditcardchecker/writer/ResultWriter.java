package org.codinjustsu.creditcardchecker.writer;


public abstract class ResultWriter {

    public abstract void write(String data);

    public void enter() {}

    public void exit() {}
}
