package org.codinjustsu.creditcardchecker.extractor;

import org.codinjustsu.creditcardchecker.exceptions.ValidationException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertArrayEquals;

public class UtilsTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void testConvertNumberAsStringToArrayOfDigits() throws Exception {
        assertArrayEquals(new int[]{1, 2, 3, 4, 5, 6}, Utils.convertNumberAsStringToArrayOfDigits("123456"));
    }

    @Test
    public void testConvertStringShouldFail() throws Exception {
        exception.expect(ValidationException.class);
        exception.expectMessage("'123a56' is not a number");
        Utils.convertNumberAsStringToArrayOfDigits("123a56");

    }
}