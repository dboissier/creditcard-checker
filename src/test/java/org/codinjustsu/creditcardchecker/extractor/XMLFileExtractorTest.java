package org.codinjustsu.creditcardchecker.extractor;

import org.codinjustsu.creditcardchecker.CreditCardEntry;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;


public class XMLFileExtractorTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();
    private XMLExtractor xmlExtractor;

    @Before
    public void setUp() throws Exception {
        xmlExtractor = new XMLExtractor();
    }

    @Test
    public void testRead_UTF8File() throws Exception {
        List<CreditCardEntry> actualCreditCards = xmlExtractor.extractCreditCards(
                new File(SeparatedValueFileExtractorTest.class.getResource("sample.xml").toURI()),
                "UTF8");

        assertReflectionEquals(
                Arrays.asList(
                        new CreditCardEntry("49927398716", "LUHN"),
                        new CreditCardEntry("49927398717", "LUHN"),
                        new CreditCardEntry("49927398717", "UNKNOWN")
                ), actualCreditCards);
    }

    @Test
    public void testRead_ISO88591File() throws Exception {
        List<CreditCardEntry> actualCreditCards = xmlExtractor.extractCreditCards(
                new File(SeparatedValueFileExtractorTest.class.getResource("sample_ISO-8859-1.xml").toURI()),
                "ISO-8859-1");

        assertReflectionEquals(
                Arrays.asList(
                        new CreditCardEntry("49927398716", "LUHN"),
                        new CreditCardEntry("49927398717", "LUHN"),
                        new CreditCardEntry("Ilétaitunefois", "LUHN")
                ), actualCreditCards);
    }
}