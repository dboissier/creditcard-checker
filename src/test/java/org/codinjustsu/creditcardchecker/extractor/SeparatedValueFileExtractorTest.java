package org.codinjustsu.creditcardchecker.extractor;

import org.codinjustsu.creditcardchecker.CreditCardEntry;
import org.junit.Test;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;
import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;

public class SeparatedValueFileExtractorTest {


    @Test
    public void testRead_UTF8File() throws Exception {
        List<CreditCardEntry> actualCreditCards = new SeparatedValueExtractor(",").extractCreditCards(
                new File(SeparatedValueFileExtractorTest.class.getResource("sample.csv").toURI()),
                "UTF8");

        assertReflectionEquals(
                Arrays.asList(
                        new CreditCardEntry("49927398716", "LUHN"),
                        new CreditCardEntry("49927398717", "LUHN"),
                        new CreditCardEntry("49927398716", "UNKNOWN"),
                        new CreditCardEntry("MyTailorIsRich", "LUHN")
                ), actualCreditCards);
    }


    @Test
    public void testRead_ISO88591File() throws Exception {
        List<CreditCardEntry> actualCreditCards = new SeparatedValueExtractor(",").extractCreditCards(
                new File(SeparatedValueFileExtractorTest.class.getResource("sample_ISO-8859-1.csv").toURI()),
                "ISO8859-1");

        assertReflectionEquals(
                Arrays.asList(
                        new CreditCardEntry("49927398716", "LUHN"),
                        new CreditCardEntry("49927398717", "LUHN"),
                        new CreditCardEntry("49927398716", "UNKNOWN"),
                        new CreditCardEntry("Ilétaitunefois", "LUHN")
                ), actualCreditCards);
    }


    @Test
    public void testReadCSVLine() throws Exception {
        SeparatedValueExtractor separatedValueFileReader = new SeparatedValueExtractor(",");
        CreditCardEntry creditCardEntry = separatedValueFileReader.parse("49927398716,LUHN");
        assertEquals("49927398716", creditCardEntry.number);
        assertEquals("LUHN", creditCardEntry.formula);
    }

    @Test
    public void testReadTSVLine() throws Exception {
        SeparatedValueExtractor separatedValueFileReader = new SeparatedValueExtractor("\t");
        CreditCardEntry creditCardEntry = separatedValueFileReader.parse("49927398716\tLUHN");
        assertEquals("49927398716", creditCardEntry.number);
        assertEquals("LUHN", creditCardEntry.formula);
    }

    @Test
    public void testReadPSVLine() throws Exception {
        SeparatedValueExtractor separatedValueFileReader = new SeparatedValueExtractor(Pattern.quote("|"));
        CreditCardEntry creditCardEntry = separatedValueFileReader.parse("49927398716|LUHN");
        assertEquals("49927398716", creditCardEntry.number);
        assertEquals("LUHN", creditCardEntry.formula);
    }
}