package org.codinjustsu.creditcardchecker.formula;


import org.codinjustsu.creditcardchecker.exceptions.ValidationException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;

public class LuhnFormulaTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    private LuhnFormula luhnFormula;

    @Before
    public void setUp() throws Exception {
        luhnFormula = new LuhnFormula();
    }

    @Test
    public void testCheckNumberSucceed() throws Exception {
        luhnFormula.validate(new int[]{4, 9, 9, 2, 7, 3, 9, 8, 7, 1, 6});
    }

    @Test
    public void testCheckNumberFail() throws Exception {
        exception.expect(ValidationException.class);
        exception.expectMessage("incorrect credit card number");
        luhnFormula.validate(new int[]{4, 9, 9, 2, 7, 3, 9, 8, 7, 1, 7});
    }


    @Test
    public void testExecuteFormula() throws Exception {
        assertEquals(70, LuhnFormula.executeFormula(new int[]{4, 9, 9, 2, 7, 3, 9, 8, 7, 1, 6}, 0));
        assertEquals(71, LuhnFormula.executeFormula(new int[]{4, 9, 9, 2, 7, 3, 9, 8, 7, 1, 7}, 0));
        assertEquals(72, LuhnFormula.executeFormula(new int[]{2, 4, 9, 9, 2, 7, 3, 9, 8, 7, 1, 6}, 0));
        assertEquals(0, LuhnFormula.executeFormula(new int[]{}, 0));
    }

    @Test
    public void testBase10() throws Exception {
        assertEquals(9, LuhnFormula.base10(9));
        assertEquals(3, LuhnFormula.base10(12));
        assertEquals(6, LuhnFormula.base10(42));

    }
}
