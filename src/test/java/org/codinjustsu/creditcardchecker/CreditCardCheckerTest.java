package org.codinjustsu.creditcardchecker;

import org.codinjustsu.creditcardchecker.writer.ResultWriter;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.net.URISyntaxException;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

public class CreditCardCheckerTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    private ResultWriter resultWriter = mock(ResultWriter.class);


    @Test
    public void testCheckFile() throws Exception {
        new CreditCardChecker(getAbsoluteFilenamePath("sample.csv"), "UTF8", resultWriter).checkFile();

        verify(resultWriter).write("49927398716|LUHN|PASS");
        verify(resultWriter).write("49927398717|LUHN|FAIL(incorrect credit card number)");
        verify(resultWriter).write("49927398716|UNKNOWN|FAIL('UNKNOWN' formula is unsupported)");
    }

    @Test
    public void testCheckUnsupportedFile() throws Exception {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("xls file is unsupported (should be csv, psv, tsv, xml)");
        new CreditCardChecker(getAbsoluteFilenamePath("unsupportedFile.xls"), "UTF8", resultWriter).checkFile();

        verify(resultWriter, never()).write(anyString());
    }

    private String getAbsoluteFilenamePath(String filename) throws URISyntaxException {
        return CreditCardCheckerTest.class.getResource(filename).toURI().getPath();
    }
}